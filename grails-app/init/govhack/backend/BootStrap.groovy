package govhack.backend

import org.springframework.beans.factory.annotation.Value

class BootStrap {

    int MAX_FILE_COUNT = 365

    @Value('${data_dir:data}')
    String dataDir

    def init = { servletContext ->
        ReadWifiDataSet()
        ReadBomDataSets()
    }

    def destroy = {
    }

    def ReadWifiDataSet() {
        if (DataSet.findByName("FREE_WIFI")) return
        def wifiFile = new File("${dataDir}/VicFreeWiFi AP Map Data 20170724.csv")
        if (!wifiFile.exists()) {
            return
        }
        def ds = new DataSet([name: "FREE_WIFI"])
        ds.save()
        if (ds.hasErrors()) {
            println "Data load of ${dir.name} failed due to: ${ds.errors}"
            return
        }

        InputStream fs = new FileInputStream(wifiFile)

        int count = 1

        fs.splitEachLine(',') { row ->
            if (count > 1) {
                Point p = new Point([name: row[1], altId: row[0], dataSet: ds, latitude: row[-4], longitude: row[-3]])
                p.save()
                if (p.hasErrors()) {
                    println "Data load of ${wifiFile.name} failed due to: ${p.errors}"
                    return
                }
            }
            count++;
        }

    }

    def ReadBomDataSets() {
        if (DataSet.findByName("BOM_2016_OBS")) return

        def dir = new File( "${dataDir}/BoM_ETA_20160501-20170430" )
        def spatialData = new File( "${dataDir}/BoM_ETA_20160501-20170430/spatial/StationData.csv" )
        if (!spatialData.exists()) {
            return
        }

        def ds = new DataSet([name: "BOM_2016_OBS"])
        ds.save()
        if (ds.hasErrors()) {
            println "Data load of ${dir.name} failed due to: ${ds.errors}"
            return
        }

        InputStream fs = new FileInputStream(spatialData)

        int count = 1
        fs.splitEachLine(',') { row ->
            if (count > 1) {
                Point p = new Point([name: row[2], altId: row[1], dataSet: ds, latitude: row[3], longitude: row[4]])
                p.save()
                if (p.hasErrors()) {
                    println "Data load of ${dir.name} failed due to: ${p.errors}"
                    return
                }
            }
            count++;
        }
        def weatherObsDir = new File( 'data/BoM_ETA_20160501-20170430/obs/' )
        def idToMax = [:]
        def idToPrecip = [:]
        int fileCount = 1
        weatherObsDir.eachFileMatch(~/.*.csv/) { weatherData ->
            if (fileCount <= MAX_FILE_COUNT) {
                fileCount++
                println "Parsing file: ${weatherData.name}"
                def idToFileMax = [:]
                def idToFilePrecip = [:]
                fs = new FileInputStream(weatherData)
                count = 1
                fs.splitEachLine(',') { row ->
                    if (count > 1) {
                        // Add all data
                        if (row[2] == "AIR_TEMP" && row[5].isDouble()) {
                            if (!idToFileMax.containsKey(row[0])) {
                                idToFileMax.put(row[0], [])
                            }
                            idToFileMax[row[0]] << Double.parseDouble(row[5])
                        }
                        if (row[2] == "PRCP" && row[5].isDouble()) {
                            if (!idToFilePrecip.containsKey(row[0])) {
                                idToFilePrecip.put(row[0], 0.0)
                            }
                            idToFilePrecip[row[0]] += Double.parseDouble(row[5])
                        }
                    }
                    count++
                }
                // TODO Doesn't handle gaps... do we have gaps?
                // Add maxes and precipitation (you could do other stuff here)
                idToFileMax.each { key, value ->
                    if (!idToMax.containsKey(key)) {
                        idToMax.put(key, [])
                    }
                    idToMax[key] << value.max()
                }
                idToFilePrecip.each { key, value ->
                    if (!idToPrecip.containsKey(key)) {
                        idToPrecip.put(key, [])
                    }
                    idToPrecip[key] << value
                }
            }
        }

        // Save values to DB
        idToMax.each { key, value ->
            println "DS Size: " + value.size()
            println "Val: " + value
            def p = Point.findByAltId(key)
            if (p) {
                p.xData = value
                p.save()
                if (p.hasErrors()) {
                    println "Data load of ${dir.name} failed due to: ${p.errors}"
                    return
                }
            }
        }
        idToPrecip.each { key, value ->
            def p = Point.findByAltId(key)
            if (p) {
                p.yData = value
                p.save()
                if (p.hasErrors()) {
                    println "Data load of ${dir.name} failed due to: ${p.errors}"
                    return
                }
            }
        }
    }
}
