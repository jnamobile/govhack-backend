package govhack.backend

import grails.transaction.Transactional
import grails.converters.JSON

@Transactional(readOnly = true)
class SearchController {

    static responseFormats = ['json', 'xml']

    def nearby() {
        def dataset = DataSet.findByName(params.datasetName)
        if (!dataset) {
            render status:404
            return
        }

        // This is not fast, hey... its a hackathon
        def result = []

        dataset.partitions.each {
            if (!(it instanceof Point)) {
                render status: 400, text: [error: 'Tried doing nearby search on non-point data'] as JSON, contentType: 'application/json'
                return
            }
            if ((Point.Distance(params.double('latitude') ?: 0, it.latitude,
                                params.double('longitude') ?: 0, it.longitude)) < params.double('distance') ?: 0) {
                Point p = new Point([name: it.name, description: it.description, longitude: it.longitude, latitude: it.latitude]);
                p.id = it.id
                if (params.from && params.to) {
                    p.xData = it.xData[params.int('from') .. params.int('to')]
                    p.yData = it.yData[params.int('from') .. params.int('to')]
                    p.stringData = it.stringData[params.int('from'), params.int('to')]
                } else {
                    p.xData = it.xData
                    p.yData = it.yData
                    p.stringData = it.stringData
                    it.comments?.each { c ->
                        p.add(c)
                    }
                }
                // Make sure we don't save
                p.discard()
                result.add(p)
            }

        }
        respond result
    }
}