package govhack.backend

import grails.rest.*

@Resource(uri='/dataset')
class DataSet {

    String name

    static hasMany = [partitions: DataPartition]

    static constraints = {
        name blank:false
    }

}