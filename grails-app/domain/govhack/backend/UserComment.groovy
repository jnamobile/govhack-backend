package govhack.backend

import grails.rest.*

@Resource(uri='/comment')
class UserComment {

    String comment
    String username
    Date dateCreated

    static belongsTo = [partition: DataPartition]

    static mapping = {
        autoTimestamp true
    }

    static constraints = {
        comment  blank:false
        username nullable:true
    }

}