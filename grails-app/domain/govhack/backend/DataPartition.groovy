package govhack.backend

import grails.rest.*

abstract class DataPartition {

    String name
    String altId

    List <Double> xData
    List <Double> yData
    List <String> stringData


    static belongsTo = [dataSet: DataSet]

    static hasMany = [comments: UserComment]

    static constraints = {
        name blank:false
    }

}