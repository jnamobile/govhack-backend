FROM anapsix/alpine-java:latest
# Based on Dockerfile from: https://github.com/emilio2hd/grails-docker

# gvm requires curl and unzip
RUN apk update && apk upgrade && apk add bash && apk add wget && apk add tar && apk --no-cache add unzip && apk add curl && apk add zip

# install sdkman
RUN curl -s http://get.sdkman.io | bash

RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh && sdk install grails 3.2.1 && sdk install gradle 2.14"

# Copy code to /app directory
COPY . /app
WORKDIR /app

RUN exec bash -l -c "source $HOME/.sdkman/bin/sdkman-init.sh && gradle clean"

RUN exec bash -l -c "source $HOME/.sdkman/bin/sdkman-init.sh && grails compile"

EXPOSE 8080

CMD exec bash -l -c "source $HOME/.sdkman/bin/sdkman-init.sh && grails run-app -Dgrails.env=production"
