

Point data
-------------

**Get Points**

`curl -H "Accept: application/json" localhost:8080/point`

**Get Point By Id**

`curl -H "Accept: application/json" localhost:8080/point/<id>`

**Get Point By Name**

`curl -H "Accept: application/json" localhost:8080/point?name=<name>`

**Search a dataset for points near a certain location**

curl -H "Accept: application/json" "http://localhost:8080/query/BOM_2016_OBS/nearby?distance=5000&latitude=-37.5&longitude=143.79&from=0&to=3"

**Search a dataset for points near a certain location filtering x,y and string data**

curl -H "Accept: application/json" "http://localhost:8080/query/BOM_2016_OBS/nearby?distance=5000&latitude=-37.5&longitude=143.79&from=0&to=30"

(This example gets results for Jan 1 to Jan 31)

User comments
---------------

** Add a comment **

curl -H "Accept: application/json" --request POST --data 'partition=1' --data 'comment=Hello World' http://localhost:8080/comment
